/*
 * unistd.c
 */

#include <config.h>
#define _GL_UNISTD_INLINE _GL_EXTERN_INLINE
#include "unistd.h"

int unistd_dummy_func() {
	return 0;
}

/* EOF */
