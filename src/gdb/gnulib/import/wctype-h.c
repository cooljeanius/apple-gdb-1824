/* wctype-h.c */
/* Normally this would be wctype.c, but that name's already taken.  */
#include <config.h>
#define _GL_WCTYPE_INLINE _GL_EXTERN_INLINE
#include "wctype.h"

int wctype_h_dummy_func() {
	return 0;
}

/* EOF */
