/*
 * math.c
 */

#include <config.h>
#define _GL_MATH_INLINE _GL_EXTERN_INLINE
#include "math.h"

int math_dummy_func() {
	return 0;
}

/* EOF */
